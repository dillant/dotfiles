# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __| | |_| | | | | (_| | | | | |_\__ \ |____/|_|_|_|\__,_|_| |_|\__|___/ Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/dillon/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# enable syntax highlighting
source ${(q-)PWD}/zsh-plugs/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#enable auto suggestions 
source ~/zsh-plugs/zsh-autosuggestions/zsh-autosuggestions.zsh
alias rm="trash -v"
alias ls="exa -aal"

#alias for git
alias addup="git add -u" alias commit="git commit -m"
alias push="git push -u origin"
alias switch="git checkout"

# alias for clear
alias cls="clear"

# alias for dilldrivebackup
alias ddb="dilldrivebackup"

# alias for vim
alias vim="nvim"

# alias for yazi
alias y="yazi"

# alias for turning on and off conservation mode
alias c="sudo nano /sys/bus/platform/drivers/ideapad_acpi/VPC2004\:00/conservation_mode"

# alias for zellij
alias zellij="zellij -l ~/.config/zellij/layout.kdl"

function nocom() {
  sed 's/^#.*//g' $1 | sed -e '/^$/d'
}

fastfetch

#PS1="[dillon@arch] "

# ~/.zshrc

eval "$(starship init zsh)"

