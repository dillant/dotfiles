" ____  _ _ _             _
"|  _ \(_) | | __ _ _ __ | |_ ___
"| | | | | | |/ _` | '_ \| __/ __|
"| |_| | | | | (_| | | | | |_\__ \
"|____/|_|_|_|\__,_|_| |_|\__|___/

" enable some things
syntax enable
set relativenumber
set noautoindent
set tabstop=2
set shiftwidth=2
set expandtab

" Vim Plug plugin manager
call plug#begin()
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
	Plug 'junegunn/fzf.vim'	
	Plug 'itchyny/lightline.vim'
	Plug 'ap/vim-css-color'
	Plug 'morhetz/gruvbox'
	Plug 'tpope/vim-fugitive'
	Plug 'preservim/nerdtree'
	Plug 'vim-scripts/AutoComplPop'
	Plug 'vim-python/python-syntax'
	Plug 'preservim/vim-markdown'
	Plug 'mattn/emmet-vim'
	Plug 'dmerejkowsky/vim-ale'
	Plug 'catppuccin/vim', { 'as': 'catppuccin' }
call plug#end()

colorscheme catppuccin_mocha
let g:lightline = { 'colorscheme': 'catppuccin_mocha' }

" enable status line
set laststatus=2

" mode is on status line so no need to show it
set noshowmode

" make esacape happen without timeout
set ttimeoutlen=50

" set leader key
let mapleader = " "

" keybinding for FZF
nnoremap <leader>. :Files<CR> 
nnoremap <leader>fh :Files ~<CR> 
" keybinding for term
nnoremap <leader>tt :term<CR>

" keybindings for window splits
nnoremap <leader>ws :split<CR>
nnoremap <leader>wv :vsplit<CR>
nnoremap <leader>wc :close<CR>
nnoremap <leader>wl :wincmd l<CR>
nnoremap <leader>wh :wincmd h<CR>
nnoremap <leader>wk :wincmd k<CR>
nnoremap <leader>wj :wincmd j<CR>

" keybinds for buffers
nnoremap <leader>bn :bnext<CR>
nnoremap <leader>bp :bprevious<CR>

" keybinding for vim fugitive
nnoremap <leader>gg :G<CR>

" keybind for nerdtree
nnoremap <leader>nt :NERDTree<CR>

" keybinding for exiting vim and writing to file
nnoremap <leader>q :wq<CR>
nnoremap <leader>fw :w<CR>
nnoremap <leader>nq :q!<CR>

" keybinding for linter
nnoremap <leader>nl :ALEFixSuggest<CR>

" enable python highlighting
let g:python_highlight_all = 1

" stop folding in markdown mode
let g:vim_markdown_folding_disabled = 1

" enable linter
let b:ale_fixers = ['prettier', 'eslint']
