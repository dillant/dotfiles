# title: Scripts
# author: Dillon King
# description: To explain what all of my scripts do

# backup.sh
to backup my system
# dilldrivebackup.sh
to backup my dilldrive folder to my external SSD
# inputs.sh
to create and input.txt for when I concatinate files with =ffmpeg=
# mount.sh
uses =rofi= to mount and unmount external drives
# orgtomd.sh
can convert basic =org= files to =markdown=
# placescripts.sh
puts scripts in /usr/local/bin/
this is for when I reinstall
# power.sh
this gives a power menu using =rofi=
# record.sh
this sets up my webcam and recording with =ffmpeg=
# vimfinder.sh
this allows me to find and opens folders in =vim= with =fzf=
# wiki.sh
this allows me to search through and open arch wiki articles locally using =rofi= and the =arch-wiki-docs= package
