#!/usr/bin/env bash

# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __|
#| |_| | | | | (_| | | | | |_\__ \
#|____/|_|_|_|\__,_|_| |_|\__|___/

wclass=$(xdotool search --class scratch)

if [[ -z $wclass ]]; then
		kitty --class scratch
else
		if [[ ! -f /tmp/scratch ]]; then
			   touch /tmp/scratch && xdo hide $wclass
		elif [[ -f /tmp/scratch ]]; then
			   rm /tmp/scratch && xdo show $wclass
		fi
fi
		 				
