#!/usr/bin/env bash

alacritty -e read -p "How many videos? " num

for ((i=1; i<${num}; i++)); do
    echo "file '$i.mkv'" >> input.txt
done
