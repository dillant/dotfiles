#!/usr/bin/env bash

scripts=$(command ls /usr/local/bin)
choice=$(printf "%s\n" "${scripts[@]}" | rofi rofi -rofi -dmenu -sb purple -nb '#282A36' -p "Script: " -l 20 -i -fn 11)

[[ -z $choice ]] && exit 0

bash /usr/local/bin/$choice
