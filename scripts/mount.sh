#!/usr/bin/env bash

function main {
    choices=("mount" "unmount")
    choice=$(printf "%s\n" "${choices[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Action: ")
    [[ -z $choice ]] && exit 1
    case $choice in
        "mount")
            shmount
            ;;
        "unmount")
        shunmount
        ;;
    esac
}

function shmount {
   drives=$(lsblk -lp | grep "part $" | awk '{print $1}')
   dmount=$(printf "%s\n" "${drives[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Drive to mount: ")
   [[ -z $dmount ]] && exit 1
   # directorys can be added
   dirs=$(find /mnt -maxdepth 3 2>/dev/null)

   mountpoint=$(printf "%s\n" "${dirs[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Mounpoint: ")

   sudo mount $dmount $mountpoint
   sudo chown -R $(whoami) $mountpoint
   if [[ $? -eq 0 ]]; then
       kitty -e whiptail --msgbox "Drive mounted" 0 0
   elif [[ $? -ne 0 ]]; then
       kitty -e whiptail --msgbox "Drive not mounted" 0 0
   fi

}

function shunmount {
   # I only mount drives to /mnt so only look there
   drives=$(lsblk -lp | grep "t /" | awk '{print $NF}' | grep "mnt")
   udrive=$(printf "%s\n" "${drives[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Drives to unmount: ")
   [[ -z $udrive ]] && exit 1
   sudo umount $udrive
   if [[ $? -eq 0 ]]; then
       kitty -e whiptail --msgbox "Drive unmounted" 0 0
   elif [[ $? -ne 0 ]]; then
       kitty -e whiptail --msgbox "Drive not unmounted" 0 0
   fi
}

# run the script
main
