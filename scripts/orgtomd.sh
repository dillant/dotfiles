#!/usr/bin/env bash

read -p "What directory: " dir

files=$(command ls $dir)

cd $dir

for file in $files; do
	orgfile=$(echo $file | awk -F "." '{print $1}')
	mv $orgfile.org $orgfile.md
	sed -i 's/*/#/g' $orgfile.md
	sed -i 's/#+begin_src/\`\`\`/' $orgfile.md
	sed -i 's/#+end_src/\`\`\`/g' $orgfile.md
	sed -i 's/#+title:/# Title:/g' $orgfile.md
	sed -i 's/#+author:/# Author:/g' $orgfile.md
done

echo "Task Finished"
