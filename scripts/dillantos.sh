#!/usr/bin/env bash

# set directory for dotfiles
DIR="~/gitlab-repos/dotfiles"

cd ~/

mkdir gitlab-repos

mv dotfiles gitlab-repos/

# install the apps
sudo pacman -S --needed - < $DIR/pkglist.txt

# copy over configs
cp $DIR/.vimrc ~/
cp $DIR/.zshrc ~/
cp -r $DIR/.config/* ~/.config/
cp -r $DIR/.themes ~/
cp -r $DIR/.icons ~/

# install vim plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# install OhMyZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# create directorys for external drives
sudo mkdir /mnt/T7
sudo mkdir /mnt/ventoy

# get wallpapers
git clone https://gitlab.com/dillant/wallpapers.git

# get herbstluftwm config
git clone https://gitlab.com/dillant/herbstluftwm-configs.git
mv herbstluftwm-configs ~/.config/herbstluftwm

# install Shodan grub theme
sudo cp -r $DIR/Shodan /boot/grub/themes/
sudo grub-mkconfig -o /boot/grub/grub.cfg instead

# set shell to zshrc
chsh -s /usr/bin/zsh 

# place the scripts
source $DIR/scripts/placescripts.sh
