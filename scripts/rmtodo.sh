#!/usr/bin/env bash

# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __|
#| |_| | | | | (_| | | | | |_\__ \
#|____/|_|_|_|\__,_|_| |_|\__|___/

tododir="/home/$USER/todo"
todofile="/home/$USER/todo/todo.txt"
[[ ! -d $tododir ]] && mkdir $tododir
[[ ! -f $todofile ]] && touch $todofile

tasks=$(cat $todofile)

[[ -z $tasks ]] && exit 1

deltask=$(printf "%s" "$tasks" | rofi -dmenu)

sed -i "/$deltask/d" $todofile
