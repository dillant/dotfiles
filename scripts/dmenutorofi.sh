#!/usr/bin/env bash

files=$(command ls)

for i in $files; do
    sed -i 's/rofi -dmenu/rofi -rofi -dmenu/g' $i
done
