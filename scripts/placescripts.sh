#!/usr/bin/env bash

# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __|
#| |_| | | | | (_| | | | | |_\__ \
#|____/|_|_|_|\__,_|_| |_|\__|___/

# dir for herbstluftwm scripts
hdir="/home/$USER/.config/herbstluftwm/scripts"

[[ -f mount.sh ]] && sudo cp mount.sh /usr/local/bin/drives
[[ -f vimplacement.sh ]] && sudo cp vimfinder.sh /usr/local/bin/vimfinder
[[ -f $hdir/scratchpad.sh ]] && sudo cp $hdir/scratchpad.sh /usr/local/bin/scratchpad
[[ -f $hdir/wiki.sh ]] && sudo cp wiki.sh /usr/local/bin/wiki
[[ -f power.sh ]] && sudo cp power.sh /usr/local/bin/power
[[ -f dilldrivebackup.sh ]] && sudo cp dilldrivebackup.sh /usr/local/bin/dilldrivebackup
[[ -f webcam.sh ]] && sudo cp webcam.sh /usr/local/bin/webcam
[[ -f record.sh ]] && sudo cp record.sh /usr/local/bin/record
[[ -f inputs.sh ]] && sudo cp inputs.sh /usr/local/bin/inputs
[[ -f scripts.sh ]] && sudo cp scripts.sh /usr/local/bin/scripts
[[ -f calc.sh ]] && sudo cp calc.sh /usr/local/bin/calc
[[ -f backup.sh ]] && sudo cp backup.sh /usr/local/bin/backup
[[ -f bsp-scratch.sh ]] && sudo cp bsp-scratch.sh /usr/local/bin/scratch
[[ -f sound-scratch.sh ]] && sudo cp sound-scratch.sh /usr/local/bin/pscratch
[[ -f files-scratch.sh ]] && sudo cp files-scratch.sh /usr/local/bin/fscratch
[[ -f todo.sh ]] && sudo cp todo.sh /usr/local/bin/todo
[[ -f addtodo.sh ]] && sudo cp addtodo.sh /usr/local/bin/addtodo
[[ -f rmtodo.sh ]] && sudo cp rmtodo.sh /usr/local/bin/rmtodo
