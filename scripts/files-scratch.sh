#!/usr/bin/env bash

# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __|
#| |_| | | | | (_| | | | | |_\__ \
#|____/|_|_|_|\__,_|_| |_|\__|___/

wclass=$(xdotool search --class files)

if [[ -z $wclass ]]; then
		kitty --class files -e yazi
else
		if [[ ! -f /tmp/files-scratch ]]; then
			   touch /tmp/files-scratch && xdo hide $wclass
		elif [[ -f /tmp/files-scratch ]]; then
			   rm /tmp/files-scratch && xdo show $wclass
		fi
fi
		 				
