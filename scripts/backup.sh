#!/usr/bin/env bash

# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __|
#| |_| | | | | (_| | | | | |_\__ \
#|____/|_|_|_|\__,_|_| |_|\__|___/

# make sure they don't exit accidently
function controlc {
    whiptail --yesno "Are you sure you want to exit?" 0 0
    [[ $? == 0 ]] && exit 0
}

# trap exit signal
trap "controlc" SIGINT

# get the mountpoint of the backup drive
lsblk
echo "What directory is the backup drive mounted to : " && read drive
echo $drive

# copy over gpg keys and passwords
sudo cp -r ~/.gnupg $drive
sudo cp -r ~/.password-store $drive

#delete outdated dotfile
rm -rf ~/gitlab-repos/dotfiles/.config
rm ~/gitlab-repos/dotfiles/.zshrc
rm ~/gitlab-repos/dotfiles/pkglist.txt

# copy over current dotfiles
cp -r ~/.config ~/gitlab-repos/dotfiles/
cp ~/.zshrc ~/gitlab-repos/dotfiles/
cp ~/.zprofile ~/gitlab-repos/dotfiles/
cp ~/.xinitrc ~/gitlab-repos/dotfiles/
cd ~/gitlab-repos/dotfiles/
paru -Qqe > pkglist.txt

# delete git repos from .config
cd ~/gitlab-repos/dotfiles/.config/
rm -rf alacritty/themes
rm -rf emacs
rm -rf herbstluftwm
rm -rf omf
rm -rf discord

# back up herbstluftwm config
cd ~/.config/herbstluftwm
git add .
git commit -m "updated"
git push -u origin main

# backup .vim and .vimrc
cp ~/.vimrc ~/gitlab-repos/dotfiles/

# push the changes to the dotfiles
cd ~/gitlab-repos/dotfiles/
git add .config
git add .bashrc
git add -u
git commit -m "updated"
git push -u origin main

# remind to push to other git repos and say script is completed
whiptail --title "Backup" --msgbox "Script completed, Remember to backup other git repos" 0 0
