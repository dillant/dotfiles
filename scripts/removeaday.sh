#!/usr/bin/env bash

current=$(grep 1095 /home/$USER/.config/polybar/config.ini | awk '{print $4}')

new=$((current - 1))

sed -i "s/Day $current/Day $new/" /home/dillon/.config/polybar/config.ini
