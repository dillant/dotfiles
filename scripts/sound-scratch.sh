#!/usr/bin/env bash

# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __|
#| |_| | | | | (_| | | | | |_\__ \
#|____/|_|_|_|\__,_|_| |_|\__|___/

wclass=$(xdotool search --class pavucontrol)

if [[ -z $wclass ]]; then
		pavucontrol
else
		if [[ ! -f /tmp/sound-scratch ]]; then
			   touch /tmp/sound-scratch && xdo hide $wclass
		elif [[ -f /tmp/sound-scratch ]]; then
			   rm /tmp/sound-scratch && xdo show $wclass
		fi
fi
		 				
