#!/usr/bin/env bash

# ____  _ _ _             _
#|  _ \(_) | | __ _ _ __ | |_ ___
#| | | | | | |/ _` | '_ \| __/ __|
#| |_| | | | | (_| | | | | |_\__ \
#|____/|_|_|_|\__,_|_| |_|\__|___/

lsblk
read -p "What drive to backup to?" drive

cd ~/
DATE=$(date +%F)
tar cvzf $DATE"_dilldrive".tar.gz ~/dilldrive
sudo mv ~/$DATE"_dilldrive".tar.gz $drive
