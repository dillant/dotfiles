#!/usr/bin/env bash


function main {
	file=$(eza -al| fzf --prompt="Open File: " --border="rounded" --reverse)
	file=$(echo $file | awk '{print $NF}')
	[[ -z $file ]] && exit 1
	[[ -f $file ]] && vim $file && exit 0
	[[ -d $file ]] && dir $file
	

}

function dir {
	dir=$1
	file=$(eza -al $dir | fzf --prompt="Open File: " --border="rounded" --reverse)
	[[ -z $file ]] && exit 1
	file=$(echo $file | awk '{print $NF}')
	file=$dir/$file
	[[ -d $file ]] && dir $file
	[[ -f $file ]] && vim $file && exit 0
}

main
