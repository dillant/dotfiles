#!/usr/bin/env bash

ffplay -window_title Webcam -fast /dev/video2 &

options=("Yes" "No")
choice=$(printf "%s\n" "${options[@]}" | rofi -dmenu -p "Would you like to start recording: ")

if [[ -z $choice ]]; then
    exit 0
elif [[ $choice == "No" ]]; then
     exit 0
elif [[ $choice == "Yes" ]]; then
    ffmpeg -y \
        -f x11grab -s 1920x1080 \
        -framerate 60 \
        -i :0.0 \
        -f alsa \
        -i default \
        out.mkv
fi
