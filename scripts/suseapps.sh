#!/usr/bin/env bash

sudo zypper in herbstluftwm starship neofetch htop btop alacritty opi eza zoxide polybar lxappearance git zellij nitrogen discord virt-manager rofi thunar parucontrol mpv tealdeer obs-studio xinput flatpak qutebrowser fontawesome-fonts NetworkManager-applet rofi-calc
